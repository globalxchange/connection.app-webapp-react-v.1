import PreregisterBox from "../../components/PreregisterBox";
import classNames from "./landingPage.module.scss";

function LandingPage(): JSX.Element {
  return (
    <div className={classNames.landingPage}>
      <PreregisterBox />
    </div>
  );
}

export default LandingPage;
